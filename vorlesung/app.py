"""This is a tiny webserver providing a simple list."""

from flask import Flask, request, jsonify


def summe(summand1, summand2):
    """Berechnet die Summe von 2 Summanden.

    :param summand1: Erster Summand
    :param summand2: Zweiter Summand
    :return: Summe beider Summanden
    """
    return summand1 + summand2


def crazy_multiply(x, y):
    """Berechnet die Multiplikation von 2 Zahlen mit einer gewissen Magie.
    Mehrzeilig.

    :param x: Faktor 1
    :param y: Faktor 2
    :return: Multiplikation der beiden Faktoren
    """
    if x == 2:
        return 2
    return x * y


app = Flask(__name__)


random_list = ["Hello", "World!"]


@app.route('/list')
def list_route():
    """Return a list with custom elements."""
    random_list.append("random")
    return jsonify(random_list)


@app.route('/')
def hello_world():
    """Return a simple hello world message."""
    return {
        "msg": "Hello World!"
    }


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
